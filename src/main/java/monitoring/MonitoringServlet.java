// Copyright (c) 2000-2015 Viaccess SA.
// Les Collines de l Arche - Tour Opera C, 92057 Paris La Defense, FRANCE
// All rights reserved.
//
// This software is the confidential and proprietary information of
// Viaccess SA. (Confidential Information). You shall not
// disclose such Confidential Information and shall use it only in
// accordance with the terms of the license agreement you entered into
// with Viaccess.
/**
 *
 */
package monitoring;

import org.apache.cxf.Bus;
import org.apache.cxf.BusFactory;
import org.apache.cxf.transport.servlet.CXFNonSpringServlet;
import org.apache.cxf.transport.servlet.CXFServlet;

import javax.servlet.ServletConfig;
import javax.xml.ws.Endpoint;

/**
 *
 */
public class MonitoringServlet extends CXFNonSpringServlet {

    /**
     *
     */
    private static final long serialVersionUID = 8583519703382396769L;

    @Override
    public void loadBus(final ServletConfig servletConfig) {
        super.loadBus(servletConfig);
        Bus bus = this.getBus();
        BusFactory.setDefaultBus(bus);
        Endpoint.publish("/version", new GetVersionImpl());
    }

}
