package monitoring;

import com.viaccess.monitoring.GetVersion;
import com.viaccess.monitoring.GetVersion_Type;

import javax.jws.WebService;

@WebService(endpointInterface = "com.viaccess.monitoring.GetVersion")
public class GetVersionImpl implements GetVersion {

    public GetVersionImpl() {
    }

    @Override
    public String getVersion(final GetVersion_Type parameters) {

        return "yes";
    }

}
